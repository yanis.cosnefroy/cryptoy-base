from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, en sortie on doit avoir une liste result tel que result[i] == (a * i + b) % n
    result = []
    for i in range(n):
        result.append((a * i + b) % n)
    return result


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, pour cela on appelle perm = compute_permutation(a, b, n) et on calcule la permutation inverse
    # result qui est telle que: perm[i] == j implique result[j] == i
    result = [0] * n
    for i in range(n):
        value = (a * i + b) % n
        result[value] = i
    return result


def encrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_permutation, str_to_unicodes et unicodes_to_str
    unicode_msg = str_to_unicodes(msg)
    res = []
    permutation = compute_permutation(a, b, 0x110000)
    for i in unicode_msg:
        encrypted_char = permutation[i]
        result = res.append(encrypted_char)
    encrypted_str = unicodes_to_str(res)
    return encrypted_str



def encrypt_optimized(msg: str, a: int, b: int) -> str:
    # A implémenter, sans utiliser compute_permutation
    res = []
    for char in msg:
        unicode_char = ord(char)
        encrypted_char = (a * unicode_char + b) % 0x110000
        res.append(encrypted_char)
    encrypted_str = unicodes_to_str(res)
    return encrypted_str


def decrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_inverse_permutation, str_to_unicodes et unicodes_to_str
    unicode_msg = str_to_unicodes(msg)
    res = []
    inverse_permutation = compute_inverse_permutation(a, b, 0x110000)
    for i in unicode_msg:
        decrypted_char = inverse_permutation[i]
        res.append(decrypted_char)
    decrypted_str = unicodes_to_str(res)
    return decrypted_str

def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    # A implémenter, sans utiliser compute_inverse_permutation
    # On suppose que a_inverse a été précalculé en utilisant compute_affine_key_inverse, et passé
    # a la fonction
    res = []
    for char in msg:
        unicode_char = ord(char)
        decrypted_char = (a_inverse * (unicode_char - b)) % 0x110000
        res.append(decrypted_char)
    decrypted_str = unicodes_to_str(res)
    return decrypted_str


def compute_affine_keys(n: int) -> list[int]:
    # A implémenter, doit calculer l'ensemble des nombre a entre 1 et n tel que gcd(a, n) == 1
    # c'est à dire les nombres premiers avec n
    keys = []

    for a in range(1, n):
        if gcd(a, n) == 1:
            keys.append(a)
    return keys


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    # Trouver a_1 dans affine_keys tel que a * a_1 % N == 1 et le renvoyer
    # Placer le code ici (une boucle)
    for key in affine_keys:
        if (a * key) % n == 1:
            return key
    # Si a_1 n'existe pas, alors a n'a pas d'inverse, on lance une erreur:
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg et b == 58

    # Placer le code ici

    target_substring = "bombe"
    b = 58
    possible_a_values = compute_affine_keys(0x110000)
    
    for a in possible_a_values:
        print(a)
        decrypted_msg = decrypt(s, a, b)
        
        if target_substring in decrypted_msg:
            return decrypted_msg, (a, b)

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg

    # Placer le code ici

    target_substring = "bombe"
    possible_a_values = compute_affine_keys(0x110000)

    for a in possible_a_values:
        ainv = compute_affine_key_inverse(a, possible_a_values, 0x110000)
        for b in range(10000):
            print(a, " = a----b = ", b)
            decrypted_msg = decrypt_optimized(s, ainv, b)
            if target_substring in decrypted_msg:
                return decrypted_msg, (a, b)

    raise RuntimeError("Failed to attack")
